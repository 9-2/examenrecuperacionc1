package com.example.examenrecu1;

import androidx.appcompat.app.AlertDialog;
        import androidx.appcompat.app.AppCompatActivity;

        import android.content.DialogInterface;
        import android.content.Intent;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombreCliente;
    private EditText txtNombre;
    private Button btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });

    }

    private void iniciarComponentes(){
        txtNombreCliente = findViewById(R.id.txtNombre);
        btnEntrar = findViewById(R.id.btnEntrar);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
    }

    // Funcion entrar
    private void entrar(){
        String textoNombre =txtNombreCliente.getText().toString().trim();

        if (textoNombre.isEmpty()) {
            Toast.makeText(this, "Introduzca el nombre del cliente", Toast.LENGTH_SHORT).show();
        }else {

            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombre.getText().toString());
            bundle.putString("nombreCliente", txtNombreCliente.getText().toString());

            Intent intent = new Intent(MainActivity.this, CotizacionActivity.class);
            intent.putExtras(bundle);

            startActivity(intent);
        }
    }



}
